#!/usr/bin/python

# passgen generates a randomized password
# Copyright (C) 2012 Ryan Porterfield

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from randstring import StringGenerator
import sys

"""
Author: Ryan Porterfield

This program generates a password for the user based on the users specified
requirements
"""

version = '1.0.3'


def gui( pass_generator ):
    import kivy
    kivy.require( '1.4.0' )
    kivy.config.Config.set('kivy', 'log_level', 'error')

    from kivy.app import App
    from kivy.config import Config
    from kivy.uix.button import Button
    from kivy.uix.textinput import TextInput
    from kivy.uix.togglebutton import ToggleButton
    from kivy.uix.widget import Widget

    Config.set('graphics', 'width', '260')
    Config.set('graphics', 'height', '450')

    class PassGenWidget( Widget ):
        btns = [
            ToggleButton( text='all', group='commands', state='down', ),
            ToggleButton( text='alphabetic', group='commands', ),
            ToggleButton( text='alpha-numeric', group='commands', ),
            ToggleButton( text='numeric', group='commands', ),
            ToggleButton( text='symbolic', group='commands', ),
        ]
        lengths = TextInput( text='length', focus=True )
        mnemonic_box = TextInput( readonly=True, text='Mnemonic', )
        password_box = TextInput( readonly=True, text='Password', )
        startbtn = Button( text='Create Password' )

        def build( self ):
            self.toggle_btns()
            self.length_field()
            self.start_btn()
            self.output()

        def toggle_btns( self ):
            y_coord = 410
            x_coord = 125
            i = 0
            for btn in self.btns:
                i += 1
                btn.height = 30
                btn.width = 115
                btn.y = y_coord
                if i % 2 != 0:
                    btn.x = 10
                else:
                    btn.x = 10 + 125
                    y_coord -= 40
                btn.bind( on_release=self.on_toggle )
                self.add_widget( btn )

        def length_field( self ):
            self.lengths.height = 30
            self.lengths.width = 115
            self.lengths.x = 135
            self.lengths.y = 330
            self.add_widget( self.lengths )

        def start_btn( self ):
            self.startbtn.width = 240
            self.startbtn.height = 50
            self.startbtn.x = 10
            self.startbtn.y = 270
            self.startbtn.bind( on_release=self.on_start )
            self.add_widget( self.startbtn )

        def output( self ):
            self.password_box.width = 240
            self.password_box.height = 80
            self.password_box.x = 10
            self.password_box.y = 180
            self.mnemonic_box.width = 240
            self.mnemonic_box.height = 160
            self.mnemonic_box.x = 10
            self.mnemonic_box.y = 10
            self.add_widget( self.password_box )
            self.add_widget( self.mnemonic_box )

        def on_toggle( self, obj ):
            pass_generator.args['command'] = obj.text

        def on_start( self, obj ):
            lengths_list = self.lengths.text.split( '; ' )
            print( lengths_list )
            pass_generator.run( lengths_list,
                                self.password_box, self.mnemonic_box )


    class PassGenApp( App ):
        def build( self ):
            ui = PassGenWidget()
            ui.build()
            return ui


    PassGenApp().run()


class PasswordGenerator():
    args = {
        'lengths' : [],
    }
    commands = [
        'all',
        'alphabetic',
        'alpha-numeric',
        'numeric',
        'symbolic',
        ]
    options = {
        '-h' : 'help',
        '--help' : 'help',
        '-v' : 'verbose',
        '--verbose' : 'verbose',
        '--version' : 'version',
        }


    def run( self, lengths, pass_box, mnem_box ):
        self.args['lengths'] = []
        for length_str in lengths:
            length_str = length_str.lower()
            base = 10
            if length_str.startswith( '0b' ):
                base = 2
                length_str = length_str[2:]
            elif length_str.startswith( '0x' ):
                base = 16
                length_str = length_str[2:]
            elif length_str.startswith( '0' ):
                base = 8
                length_str = length_str[1:]

            try:
                self.args['lengths'].append( int( length_str, base ) )
            except ValueError as e:
                print e

        charset = 'all'
        if 'command' in self.args.keys():
            charset = self.args['command']

        str_generator = StringGenerator()
        passwords = str_generator.create_string(
            self.args['lengths'],
            string_type=charset
        )

        pairs = self.mnemonic( passwords )
        pass_str = ''
        mnem_str = ''
        for pair in pairs:
            print pair[0], '\n', pair[1]
            pass_str = pass_str + pair[0] + '\n'
            mnem_str = mnem_str + pair[1] + '\n\n'
        pass_box.text = pass_str
        mnem_box.text = mnem_str

    def parse_args( self ):
        if not len( sys.argv ) > 1:
            self.args['gui'] = True
        else:
            for arg in sys.argv[1:]:
                if ( arg in self.commands ) and ( 'command' not in self.args ):
                    self.args['command'] = arg
                elif ( arg in self.commands )  and ( 'command' in self.args ):
                    self.print_usage( None, exit=True )
                elif arg in self.options.keys():
                    self.args[options[arg]] = True
                elif arg == 'gui':
                    self.args['gui'] = True
                else:
                    try:
                        self.args['lengths'].append( int( arg ) )
                    except ValueError:
                        self.print_usage(
                            'ERROR: ' + arg + ' is not a valid option',
                            exit=True
                        ) 

            if len( self.args['lengths'] ) < 1:
                self.print_usage(
                    'ERROR: No password lengths provided.',
                    exit=True
                )

        return self.args


    def mnemonic( self, passwords ):
        """
        DOCSTRING
        """
        pairs = []
        # String constant that maps letters of the alphabet to it's military 
        # (phonetic) alphabet counterpart
        phonetic_alphabet = {
            'A' : 'ALPHA', 'a' : 'alpha',
            'B' : 'BRAVO', 'b' : 'bravo',
            'C' : 'CHARLIE', 'c' : 'charlie',
            'D' : 'DELTA', 'd' : 'delta',
            'E' : 'ECHO', 'e' : 'echo',
            'F' : 'FOXTROT', 'f' : 'foxtrot',
            'G' : 'GOLF', 'g' : 'golf',
            'H' : 'HOTEL', 'h' : 'hotel',
            'I' : 'INDIA', 'i' : 'india',
            'J' : 'JULIETE', 'j' : 'juliet',
            'K' : 'KILO', 'k' : 'kilo',
            'L' : 'LIMA', 'l' : 'lima',
            'M' : 'MIKE', 'm' : 'mike',
            'N' : 'NOVEMBER', 'n' : 'november',
            'O' : 'OSCAR', 'o' : 'oscar',
            'P' : 'PAPA', 'p' : 'papa',
            'Q' : 'QUEBEC', 'q' : 'quebec',
            'R' : 'ROMEO', 'r' : 'romeo',
            'S' : 'SIERRA', 's' : 'sierra',
            'T' : 'TANGO', 't' : 'tango',
            'U' : 'UNIFORM', 'u' : 'uniform',
            'V' : 'VICTOR', 'v' : 'victor',
            'W' : 'WHISKEY', 'w' : 'whiskey',
            'X' : 'X-RAY', 'x' : 'x-ray',
            'Y' : 'YO-YO', 'y' : 'yo-yo',
            'Z' : 'ZULU', 'z' : 'zulu'
        }

        for password in passwords:
            # The string that will help the user remember their password
            mnemonic = ''
            for char in password:
                if char in phonetic_alphabet:
                    mnemonic = mnemonic + phonetic_alphabet[char] + ' '
                else:
                    mnemonic = mnemonic + char + ' '

            pairs.append( (password, mnemonic ) )
        return pairs


def main():
    # Print a blank line for readability
    print ''
    pass_generator = PasswordGenerator()
    args = pass_generator.parse_args()
    if 'gui' in args:
       gui( pass_generator ) 
    else:
        if 'command' in args:
            charset = args['command']
        else:
            charset = 'all'
        str_generator = StringGenerator(verbose=( 'verbose' in args ) )
        passwords = str_generator.create_string(
            args['lengths'],
            string_type=charset
        )

        if not passwords:
            print >> sys.stderr, 'No passwords created'
            sys.exit(1)

        print_password( pass_generator.mnemonic(passwords) )
        # Print another blank line at the end of the program
        print ''


def print_password( pairs ):
    pass_str = 'Here\'s your password:\n\t'
    mnem_str = 'Here\'s a mnemonic to help you remember your password:\n\t'
    for pair in pairs:
        print pass_str, pair[0]
        print mnem_str, pair[1]


def print_usage( message, exit=False, exit_code=1 ):
    usage = """
usage: ./passgen.py [--help] [--version] [options] [character set] length...
options
        -h, --help          prints the help documentation
        -v, --verbose       verbose mode; program prints extra debug info
        --version           prints the version number
character set
        all                 All characters are valid
        alphabetic          Only A-Z and a-z
        alpha-numeric       A-Z, a-z, and 0-9
        numeric             0-9 only
        symbolic            ASCII symbols
length
        A list of length(s) of the passwords to be generated
"""
    if message:
        print( message )
    print( usage )

    if exit:
        sys.exit( exit_code )


# Boilerplate syntax to call main function
# Because it's conventional
if __name__ == '__main__':
    main()

#
