# randstring is a python module that generates a randomized string
# Copyright (C) 2012 Ryan Porterfield

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import random


class StringGenerator():
    """
    Docstring
    """
    __verbose = False
    __min = 0
    __max = 92

    def __init__(self, verbose=False):
        __verbose = verbose

    def create_string(self, lengths, string_type='all'):
        """
        Generate a random str of specified length, and type.
        """
        _strings = []

        if string_type == 'all':
            print('Creating a random string with all characters valid.')
        elif string_type == 'alphabetic':
            self.__min = 10
            self.__max = 61
            print('Creating a random string with only alphabetic characters.')
        elif string_type == 'alpha-numeric':
            self.__max = 61
            print('Creating a random string with only alphanumeric characters.')
        elif string_type == 'numeric':
            self.__max = 9
            print('Creating a random string with only numeric characters.')
        elif string_type == 'symbolic':
            self.__min = 62
            print('Creating a random string with only symbolic characters.')
        else:
            print(string_type, 'is an invalid value for "string_type"')
            return

        for length in lengths:
            rand_str = ''
            for index in range(length):
                rand_str = rand_str + self.getRand(self.__min, self.__max)
            _strings.append(rand_str)

        return _strings

    def getRand(self, min, max):
        """
        DOCSTRING
        """
        CHAR_SET = {
            0 : '0', 1 : '1', 2 : '2', 3 : '3', 4 : '4', 5 : '5', 6 : '6',
            7 : '7', 8 : '8', 9 : '9', 10 : 'a', 11 : 'b', 12 : 'c', 13 : 'd',
            14 : 'e', 15 : 'f', 16 : 'g', 17 : 'h', 18 : 'i', 19 : 'j',
            20 : 'k', 21 : 'l', 22 : 'm', 23 : 'n', 24 : 'o', 25 : 'p',
            26 : 'q', 27 : 'r', 28 : 's', 29 : 't', 30 : 'u', 31 : 'v',
            32 : 'w', 33 : 'x', 34 : 'y', 35 : 'z', 36 : 'A', 37 : 'B',
            38 : 'C', 39 : 'D', 40 : 'E', 41 : 'F', 42 : 'G', 43 : 'H',
            44 : 'I', 45 : 'J', 46 : 'K', 47 : 'L', 48 : 'M', 49 : 'N',
            50 : 'O', 51 : 'P', 52 : 'Q', 53 : 'R', 54 : 'S', 55 : 'T',
            56 : 'U', 57 : 'V', 58 : 'W', 59 : 'X', 60 : 'Y', 61 : 'Z',
            62 : '!', 63 : '@', 64 : '#', 65 : '$', 66 : '^', 67 : '&',
            68 : '*', 69 : '(', 70 : ')', 71 : '-', 72 : '_', 73 : '=',
            74 : '+', 75 : '`', 76 : '~', 77 : '[', 78 : '{', 79 : ']',
            80 : '}', 81 : '\\', 82 : '|', 83 : ';', 84 : ':', 85 : '"',
            86 : "'", 87 : ',', 88 : '<', 89 : '.', 90 : '>', 91 : '/',
            92 : '?'
        }
        char = CHAR_SET[random.randint(min, max)]
        return char

#
